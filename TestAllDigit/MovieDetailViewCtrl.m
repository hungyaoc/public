//
//  MovieDetailViewCtrl.m
//  TestAllDigit
//
//  Created by Jeff Chen on 5/15/14.
//  Copyright (c) 2014 Jeff Chen. All rights reserved.
//

#import "MovieDetailViewCtrl.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MovieDetailViewCtrl ()

@end

@implementation MovieDetailViewCtrl
@synthesize dicMovie;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
}

-(void)setupUI
{
    // movie title
    self.title = [self.dicMovie objectForKey:@"trackName"];
    
    // release date => 2013-11-27T08:00:00Z
    NSString* dateString = [self.dicMovie objectForKey:@"releaseDate"];
    
    NSArray* arry = [dateString componentsSeparatedByString:@"T"];
    
    //NSString *dateString = @"28042013";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *dt = [formatter dateFromString:arry[0]];
    
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateAsString = [formatter stringFromDate:dt];
    
    NSLog(@"%@", dateAsString);
    
    
    
    
    self.lblReleaseDate.text = dateAsString;
    
    // rating
    self.lblRating.text = [self.dicMovie objectForKey:@"contentAdvisoryRating"];
    
    // price
    NSString* strPrice      = [self.dicMovie objectForKey:@"collectionPrice"];
    NSString* strCurrency   = [self.dicMovie objectForKey:@"currency"];
    
    self.lblPrice.text = [NSString stringWithFormat:@"%@/%@", strPrice, strCurrency];
    
    // long description
    self.lblDesp.text = [self.dicMovie objectForKey:@"longDescription"];
    
    
    
    
    // image
    NSString* strURL = [self.dicMovie objectForKey:@"artworkUrl100"];
    NSURL *imageURL = [NSURL URLWithString:strURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            self.imgArtwork.image = [UIImage imageWithData:imageData];
        });
    });
}

-(void)viewDidLayoutSubviews
{
    /*CGRect rc = self.viewPreview.frame;
    
    // preview => previewUrl
    NSString* strPath = [self.dicMovie objectForKey:@"previewUrl"];
    NSURL *movieURL = [NSURL URLWithString:strPath];
    movieController = [[MPMoviePlayerViewController alloc] initWithContentURL:movieURL];
    movieController.view.frame = rc;
    
    [self.view addSubview:movieController.view];
    
    //[self presentMoviePlayerViewControllerAnimated:movieController];
    [movieController.moviePlayer play];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBuy:(id)sender
{
    NSString* strURL = [self.dicMovie objectForKey:@"collectionViewUrl"];
    
    if([strURL length] == 0)
    {
        strURL = [self.dicMovie objectForKey:@"trackViewUrl"];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
}

- (IBAction)goPreview:(id)sender
{
    NSString* strURL = [self.dicMovie objectForKey:@"previewUrl"];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strURL]];
}
@end
