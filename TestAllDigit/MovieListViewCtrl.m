//
//  MovieListViewCtrl.m
//  TestAllDigit
//
//  Created by Jeff Chen on 5/15/14.
//  Copyright (c) 2014 Jeff Chen. All rights reserved.
//

#import "MovieListViewCtrl.h"
#import "MovieDetailViewCtrl.h"
#import "SBJson.h"

@interface MovieListViewCtrl ()

@end

@implementation MovieListViewCtrl
@synthesize strMovieName;
@synthesize myTable;
@synthesize arrayMovie;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self download_MovieList];
    
    self.title = @"Search Result";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark table delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayMovie count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Fix for iOS 7 to clear backgroundColor
    //cell.backgroundColor = [UIColor clearColor];
    //cell.backgroundView = [UIView new];
    //cell.selectedBackgroundView = [UIView new];
    
    // Configure the cell.
    NSDictionary* dic = [self.arrayMovie objectAtIndex:indexPath.row];
    cell.textLabel.text = [dic objectForKey:@"trackName"];
    //cell.textLabel.textColor = [UIColor whiteColor];
    
    
    // image
    NSString* strURL = [dic objectForKey:@"artworkUrl30"];
    NSURL *imageURL = [NSURL URLWithString:strURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            cell.imageView.image = [UIImage imageWithData:imageData];
        });
    });
    
    cell.detailTextLabel.text = [dic objectForKey:@"contentAdvisoryRating"];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieDetailViewCtrl* dlg = [[MovieDetailViewCtrl alloc] init];
    
    dlg.dicMovie = [self.arrayMovie objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:dlg animated:YES];
}


#pragma mark
#pragma mark download
-(void)download_MovieList
{
    NSString* strURL = [NSString stringWithFormat:@"https://itunes.apple.com/search?term=\"%@\"", self.strMovieName];
    
    strURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(conn == nil)
    {
        NSLog(@"netowrk error!");
    }
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString* myString = [[NSString alloc] initWithData:_responseData encoding:NSUTF8StringEncoding];
    
    //debug
    NSLog(@"%@", myString);
    
    // parse
    // Create SBJSON object to parse JSON
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    
    // parse the JSON string into an object - assuming json_string is a NSString of JSON data
    NSDictionary *object = [parser objectWithString:myString];
    
    NSNumber* count = [object objectForKey:@"resultCount"];
    
    if([count intValue] > 0)
    {
        self.arrayMovie = [object objectForKey:@"results"];
        
        [self.myTable reloadData];
    }
    else
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"no match result!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
}

- (IBAction)goClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
