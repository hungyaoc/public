//
//  MovieListViewCtrl.h
//  TestAllDigit
//
//  Created by Jeff Chen on 5/15/14.
//  Copyright (c) 2014 Jeff Chen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieListViewCtrl : UIViewController <UITableViewDataSource, UITableViewDelegate, NSURLConnectionDelegate>
{
    NSMutableData *_responseData;
}

@property(nonatomic, strong) IBOutlet UITableView* myTable;
@property(nonatomic, strong) NSMutableArray* arrayMovie;
@property(nonatomic, strong) NSString* strMovieName;


@end
