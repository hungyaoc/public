//
//  MovieDetailViewCtrl.h
//  TestAllDigit
//
//  Created by Jeff Chen on 5/15/14.
//  Copyright (c) 2014 Jeff Chen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MovieDetailViewCtrl : UIViewController
{
    MPMoviePlayerViewController* movieController;
}  

@property(nonatomic, strong) NSDictionary* dicMovie;
@property (strong, nonatomic) IBOutlet UILabel *lblRating;
@property (strong, nonatomic) IBOutlet UIImageView *imgArtwork;
@property (strong, nonatomic) IBOutlet UILabel *lblReleaseDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UITextView *lblDesp;
@property (strong, nonatomic) IBOutlet UIView *viewPreview;

- (IBAction)goBuy:(id)sender;
- (IBAction)goPreview:(id)sender;

@end
