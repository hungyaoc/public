//
//  ViewController.m
//  TestAllDigit
//
//  Created by Jeff Chen on 5/15/14.
//  Copyright (c) 2014 Jeff Chen. All rights reserved.
//

#import "ViewController.h"
#import "MovieListViewCtrl.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startSearch:(id)sender
{
    NSString* titleName =  self.edit_name.text;
    
    if([titleName length] > 0)
    {
        MovieListViewCtrl* dlg = [[MovieListViewCtrl alloc] init];
        
        dlg.strMovieName = titleName;
        
        UINavigationController* ctrl = [[UINavigationController alloc] initWithRootViewController:dlg];
        
        [self presentViewController:ctrl animated:YES completion:nil];

    }
    else
    {
        // error ... title can not be empty
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Please input your movie name"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        [alert show];
    }
}
@end
